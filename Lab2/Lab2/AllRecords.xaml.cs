﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Net;
using OfficeOpenXml;
using Microsoft.Win32;
using System.Diagnostics;

namespace Lab2
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class AllRecords : Window
    {
        public static List<Record> records = new List<Record>(); // Здесь хранятся все записи из excel таблицы

        static int NumberOfRecordsOnList = 10; // Кол-во записей на одной странице
        List<Record> currentRecords = new List<Record>(NumberOfRecordsOnList); // Некий буфер записей, передающийся в DataGrid в зависимости от текущий страницы
        int currentListNumber = 0; // текующая страница
        int CurrentListNumber
        {
            get => currentListNumber;
            set
            {
                currentListNumber = value;
                UseButton();
            }
        }
        int MaxLists; // Всего страниц
        bool checker = false;


        private void AllRecords_Loaded(object sender, RoutedEventArgs e)
        {
            FileInfo fi = null;
            string pathData = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "lab2korolev", "data1.xlsx");
            string pathDirectory = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "lab2korolev");
            if (File.Exists(pathData))
            {
                if (MessageBox.Show("База данных была успешно найдена! Продолжить работу?",
                            "Оповещение", MessageBoxButton.YesNo) != MessageBoxResult.Yes)
                {
                    Process.GetCurrentProcess().Kill();
                }
                fi = new FileInfo(pathData);
            }
            else
            {
                if (MessageBox.Show("Файл с локальной базой данных не был найден! Хотите произвести первоначальную загрузку?",
                    "Оповещение", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {
                    if (!Directory.Exists(pathDirectory))
                    {
                        Directory.CreateDirectory(pathDirectory);
                    }
                    try
                    {
                        using (WebClient webClient = new WebClient())
                        {
                            webClient.DownloadFile("https://bdu.fstec.ru/documents/files/thrlist.xlsx",
                               pathData);
                            if (MessageBox.Show("База данных была успешно загружена! Продолжить работу?",
                                "Оповещение", MessageBoxButton.YesNo) != MessageBoxResult.Yes)
                            {
                                Process.GetCurrentProcess().Kill();
                            }
                        }
                        fi = new FileInfo(pathData);
                    }
                    catch
                    {
                        MessageBox.Show("Не удалось скачать файл!", "Ошибка");
                        Process.GetCurrentProcess().Kill();
                    }
                }

            }

            records = ExcelParse(fi); // Заполнение records

            foreach (var el in records) // исправление проблем с представлением строк в excel
            {
                el.Name = el.Name.Replace("\r", "");
                el.Name = el.Name.Replace("_x000d_", "");
                el.Description = el.Description.Replace("\r", "");
                el.Description = el.Description.Replace("_x000d_", "");
                el.Source = el.Source.Replace("\r", "");
                el.Source = el.Source.Replace("_x000d_", "");
                el.Object = el.Object.Replace("\r", "");
                el.Object = el.Object.Replace("_x000d_", "");
            }


            // Загрузка начальной страницы
            MaxLists = (records.Count / NumberOfRecordsOnList);
            if (records.Count % NumberOfRecordsOnList != 0)
            {
                MaxLists++;
                checker = true;
            }
            if (records.Count == 0)
            {
                ButtonPrev.IsEnabled = false;
                ButtonNext.IsEnabled = false;
            }
            else
            {
                CurrentListNumber++;
                ButtonPrev.IsEnabled = true;
                ButtonNext.IsEnabled = true;
            }
        }
        /// <summary>
        /// Заполняет коллекцию записями из excel таблицы
        /// </summary>
        public static List<Record> ExcelParse(FileInfo fileInfo)
        {
            var records_collection = new List<Record>();
            using (ExcelPackage excelPackage = new ExcelPackage(fileInfo))
            {
                ExcelWorksheet MainWorkSheet = excelPackage.Workbook.Worksheets[1];

                int i = 3;
                int j = 1;
                try
                {
                    while (MainWorkSheet.Cells[i, 1].Value != null)
                    {
                        records_collection.Add(new Record(Convert.ToInt32(MainWorkSheet.Cells[i, j].Value),
                            MainWorkSheet.Cells[i, j + 1].Value.ToString(),
                            MainWorkSheet.Cells[i, j + 2].Value.ToString(),
                            MainWorkSheet.Cells[i, j + 3].Value.ToString(),
                            MainWorkSheet.Cells[i, j + 4].Value.ToString(),
                            Convert.ToBoolean(MainWorkSheet.Cells[i, j + 5].Value),
                            Convert.ToBoolean(MainWorkSheet.Cells[i, j + 6].Value),
                            Convert.ToBoolean(MainWorkSheet.Cells[i, j + 7].Value)));
                        i++;
                    }
                }
                catch
                {
                    MessageBox.Show("Не удалось корректно обработать таблицу", "Ошибка!");
                    Process.GetCurrentProcess().Kill();
                }
                return records_collection;
            }
        }
        /// <summary>
        /// Метод для обработки событий нажатия на стрелки "вправо", "влево" (перелистывания страниц)
        /// </summary>
        private void UseButton()
        {
            CountLists.Text = $"{CurrentListNumber} / {MaxLists}";
            currentRecords.Clear();
            if (CurrentListNumber == MaxLists && records.Count != 0)
            {
                int x = records.Count % NumberOfRecordsOnList;
                if (!checker)
                {
                    x = NumberOfRecordsOnList;
                }
                for (int i = records.Count - x;
                    i < records.Count; i++)
                {
                    currentRecords.Add(records[i]);
                }
            }
            else if(records.Count != 0)
            {
                for (int i = CurrentListNumber * NumberOfRecordsOnList - NumberOfRecordsOnList;
                    i < CurrentListNumber * NumberOfRecordsOnList; i++)
                {
                    currentRecords.Add(records[i]);
                }
            }
                MainGrid.ItemsSource = null;
                MainGrid.ItemsSource = currentRecords;
        }

        
        private void Button_Click_Next(object sender, RoutedEventArgs e)
        {
            if(CurrentListNumber == MaxLists)
            {
                CurrentListNumber = 1;
            }
            else
            {
                CurrentListNumber++;
            }
        }

        private void Button_Click_Back(object sender, RoutedEventArgs e)
        {
            if (CurrentListNumber == 1)
            {
                CurrentListNumber = MaxLists;
            }
            else
            {
                CurrentListNumber--;
            }
        }

        private void OnKeyDownSpecialID(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                TextBox textBox = (TextBox)sender;
                try
                {
                    int x = int.Parse(textBox.Text);
                    if (records.Any(el => el.ID == x))
                    {
                        foreach(var el in MainGrid.Columns)
                        {
                            if (el is DataGridTextColumn)
                            {
                                (el as DataGridTextColumn).FontSize = 20;
                            }
                        }
                        ButtonPrev.IsEnabled = false;
                        ButtonNext.IsEnabled = false;
                        currentRecords.Clear();
                        currentRecords.Add(records.First(el => (el.ID == x)));
                        MainGrid.ItemsSource = null;
                        MainGrid.ItemsSource = currentRecords;
                    }
                    else if(x == 0)
                    {
                        foreach (var el in MainGrid.Columns)
                        {
                            if (el is DataGridTextColumn)
                            {
                                (el as DataGridTextColumn).FontSize = 10;
                            }
                        }
                        ButtonPrev.IsEnabled = true;
                        ButtonNext.IsEnabled = true;
                        CurrentListNumber = 1;
                    }
                    else
                    {
                        throw new Exception("Записи с таким ID не существует!");
                    }
                    
                }
                catch(FormatException ex)
                {
                    MessageBox.Show(ex.Message,"Ошибка");
                }
                catch(Exception ex)
                {
                    MessageBox.Show(ex.Message,"Ошибка");
                }
            }
        }
 
        private void OnKeyDownNumberOfRecords(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                TextBox textBox = (TextBox)sender;
                try
                {
                    int x = int.Parse(textBox.Text);
                    if(x <= 0 || x > records.Count)
                    {
                        throw new Exception("Столько записей выводить нельзя!");
                    }
                    NumberOfRecordsOnList = x;

                    MaxLists = (records.Count / NumberOfRecordsOnList);
                    if (records.Count % NumberOfRecordsOnList != 0)
                    {
                        MaxLists++;
                        checker = true;
                    }
                    else
                    {
                        checker = false;
                    }
                    CurrentListNumber = 1;
                    CountLists.Text = $"{CurrentListNumber} / {MaxLists}";

                    currentRecords.Clear();
                    for (int i = CurrentListNumber * NumberOfRecordsOnList - NumberOfRecordsOnList;
                    i < CurrentListNumber * NumberOfRecordsOnList; i++)
                    {
                        currentRecords.Add(records[i]);
                    }
                    MainGrid.ItemsSource = null;
                    MainGrid.ItemsSource = currentRecords;

                }
                catch (FormatException ex)
                {
                    MessageBox.Show(ex.Message, "Ошибка");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Ошибка");
                }
            }
        }

        // CheckBox - краткая запись (активный)
        private void IfShortRecords(object sender, RoutedEventArgs e)
        {
            var column0 = (DataGridTextColumn)MainGrid.Columns[0];
            column0.FontSize = 20;
            var column1 = (DataGridTextColumn)MainGrid.Columns[1];
            column1.FontSize = 20;
            column1.Width = new DataGridLength(1, DataGridLengthUnitType.Star);
            MainGrid.Columns[2].Visibility = Visibility.Hidden;
            MainGrid.Columns[3].Visibility = Visibility.Hidden;
            MainGrid.Columns[4].Visibility = Visibility.Hidden;
            MainGrid.Columns[5].Visibility = Visibility.Hidden;
        }
        // CheckBox - краткая запись (не активный)
        private void IfNotShortRecords(object sender, RoutedEventArgs e)
        {
            var column0 = (DataGridTextColumn)MainGrid.Columns[0];
            column0.FontSize = 10;
            var column1 = (DataGridTextColumn)MainGrid.Columns[1];
            column1.FontSize = 10;
            column1.Width = new DataGridLength(150, DataGridLengthUnitType.Pixel);
            MainGrid.Columns[2].Visibility = Visibility.Visible;
            MainGrid.Columns[3].Visibility = Visibility.Visible;
            MainGrid.Columns[4].Visibility = Visibility.Visible;
            MainGrid.Columns[5].Visibility = Visibility.Visible;
        }

        private void Button_Click_Changes(object sender, RoutedEventArgs e)
        {
            Changes changes = new Changes(records);
            if (Changes.recordsBefore.Count == 0 && Changes.recordsAdded.Count == 0 && Changes.recordsDeleted.Count == 0)
            {
                MessageBox.Show("Новая таблица либо не была загружена, либо в ней нет изменений!","Оповещение");
            }
            else
            {
                changes.Closed += Changes_Closed;
                changes.ShowDialog();
            }
        }

        private void Changes_Closed(object sender, EventArgs e)
        {
            MaxLists = (records.Count / NumberOfRecordsOnList);
            if (records.Count % NumberOfRecordsOnList != 0)
            {
                MaxLists++;
                checker = true;
            }
            CurrentListNumber = 1;
            ButtonPrev.IsEnabled = true;
            ButtonNext.IsEnabled = true;
        }

        private void Button_Click_Exit(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Вы действительно хотите закончить работу приложения?",
                            "Оповещение", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                Process.GetCurrentProcess().Kill();
            }
        }
    }
}
