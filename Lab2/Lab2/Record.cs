﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Lab2
{
    public class Record
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Source { get; set; }
        public string Object { get; set; }
        public bool ConfidentViolation { get; set; }
        public bool IntegrityViolation { get; set; }
        public bool AccessViolation { get; set; }

        public Record(int id, string name, string description, string source, string obj, bool conf, bool integr, bool access)
        {
            ID = id;
            Name = name;
            Description = description;
            Source = source;
            Object = obj;
            ConfidentViolation = conf;
            IntegrityViolation = integr;
            AccessViolation = access;
        }

        public override bool Equals(object obj)
        {
            if (obj is Record obj_record)
            {
                return ID == obj_record.ID
                    && Name == obj_record.Name
                    && Description == obj_record.Description
                    && Source == obj_record.Source
                    && Object == obj_record.Object
                    && ConfidentViolation == obj_record.ConfidentViolation
                    && IntegrityViolation == obj_record.IntegrityViolation
                    && AccessViolation == obj_record.AccessViolation;
            }
            else
            {
                return false;
            }
        }

    }
}
