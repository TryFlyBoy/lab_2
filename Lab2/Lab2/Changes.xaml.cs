﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;
using System.Net;
using System.Diagnostics;

namespace Lab2
{
    /// <summary>
    /// Логика взаимодействия для Changes.xaml
    /// </summary>
    public partial class Changes : Window
    {
        List<Record> NewRecords = new List<Record>(); // коллекция всех новых записей
        //List<Record> OldRecords = new List<Record>(); // коллекция всех старых записей

        public static List<Record> recordsBefore = new List<Record>(); // коллекция измененных записей из старой коллекции
        public static List<Record> recordsAfter = new List<Record>(); // коллекция измененных записей из новой коллекции
        public static List<Record> recordsAdded = new List<Record>(); // коллекция добавленных записей
        public static List<Record> recordsDeleted = new List<Record>(); // коллекция удаленных записей

        public Changes() { }

        public Changes(List<Record> oldrecords)
        {
            InitializeComponent();

            string pathDataNew = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "lab2korolev", "data2.xlsx");
            string pathDataOld = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "lab2korolev", "data1.xlsx");
            FileInfo fi;
            try
            {
                using (WebClient webClient = new WebClient())
                {
                    webClient.DownloadFile("https://bdu.fstec.ru/documents/files/thrlist.xlsx",
                       pathDataNew);
                }
            }
            catch
            {
                MessageBox.Show("Не удалось скачать файл", "Ошибка!");
                Process.GetCurrentProcess().Kill();
            }
            fi = new FileInfo(pathDataNew);

            NewRecords = AllRecords.ExcelParse(fi);

            foreach (var el in NewRecords) 
            {
                el.Name = el.Name.Replace("\r", "");
                el.Name = el.Name.Replace("_x000d_", "");
                el.Description = el.Description.Replace("\r", "");
                el.Description = el.Description.Replace("_x000d_", "");
                el.Source = el.Source.Replace("\r", "");
                el.Source = el.Source.Replace("_x000d_", "");
                el.Object = el.Object.Replace("\r", "");
                el.Object = el.Object.Replace("_x000d_", "");
            }

            // Из новой таблицы делаем текущюю и заменяем имя файла новой таблицы на текущую
            AllRecords.records = NewRecords;
            try
            {
                File.Delete(pathDataOld);
                File.Move(pathDataNew, pathDataOld);
            }
            catch
            {
                MessageBox.Show("Закройте, пожалуйста, Excel файл!", "Ошибка");
                Close();
                return;
            }

            // -----------------------------------------------------------------------------

            foreach (var x in oldrecords)
            {
                if (!NewRecords.Any(el => (el.ID == x.ID)))
                {
                    recordsDeleted.Add(x);
                }
            }
            foreach (var x in NewRecords)
            {
                if (oldrecords.Any(el => (el.ID == x.ID)))
                {
                    var record = oldrecords.First(el => (el.ID == x.ID));
                    if (!(x.Equals(record)))
                    {
                        recordsBefore.Add(record);
                        recordsAfter.Add(x);
                    }
                }
                else
                {
                    recordsAdded.Add(x);
                }
            }
        }

        private void Changes_Loaded(object sender, RoutedEventArgs e)
        {
            GridBefore.ItemsSource = recordsBefore;
            GridAfter.ItemsSource = recordsAfter;
        }

        /// <summary>
        /// Показывает измененнные записи
        /// </summary>
        private void Button_Click_ChangesRecords(object sender, RoutedEventArgs e)
        {
            RowForGridAfter.Height = new GridLength(1, GridUnitType.Star);
            GridAfter.Visibility = Visibility.Visible;
            TextBlockBecame.Visibility = Visibility.Visible;
            TextBlockWas.Text = "Было:";
            GridBefore.ItemsSource = null;
            GridAfter.ItemsSource = null;
            GridBefore.ItemsSource = recordsBefore;
            GridAfter.ItemsSource = recordsAfter;
            ButtonChangesRecords.IsChecked = true;
            ButtonAddedRecords.IsChecked = false;
            ButtonDeletedRecords.IsChecked = false;
        }
        /// <summary>
        /// Показывает добавленные записи
        /// </summary>
        private void Button_Click_AddedRecords(object sender, RoutedEventArgs e)
        {
            RowForGridAfter.Height = GridLength.Auto;
            GridBefore.ItemsSource = null;
            GridBefore.ItemsSource = recordsAdded;
            GridAfter.Visibility = Visibility.Hidden;
            TextBlockBecame.Visibility = Visibility.Hidden;
            TextBlockWas.Text = "Добавленные записи:";
            ButtonChangesRecords.IsChecked = false;
            ButtonAddedRecords.IsChecked = true;
            ButtonDeletedRecords.IsChecked = false;
        }
        /// <summary>
        /// Показывает удаленные записи
        /// </summary>
        private void Button_Click_DeletedRecords(object sender, RoutedEventArgs e)
        {
            RowForGridAfter.Height = GridLength.Auto;
            GridBefore.ItemsSource = null;
            GridBefore.ItemsSource = recordsDeleted;
            GridAfter.Visibility = Visibility.Hidden;
            TextBlockBecame.Visibility = Visibility.Hidden;
            TextBlockWas.Text = "Удаленные записи:";
            ButtonChangesRecords.IsChecked = false;
            ButtonAddedRecords.IsChecked = false;
            ButtonDeletedRecords.IsChecked = true;
        }
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            recordsAdded.Clear();
            recordsDeleted.Clear();
            recordsBefore.Clear();
            recordsAfter.Clear();
            
        }
    }
}
